import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui';

//import './element-ui/index.css';
import locale from 'element-ui/lib/locale/lang/ru-RU'

import store from './store'
import router from './router'

import Chartkick from 'vue-chartkick'
import Chart from 'chart.js'

Vue.use(Chartkick.use(Chart))
Vue.use(ElementUI, { locale })
Vue.use(ElementUI);
Vue.config.productionTip = false

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
