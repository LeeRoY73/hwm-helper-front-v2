import Vue from 'vue'
import VueRouter from 'vue-router'

import NotUserTopBar from "@/components/notUserTopBar/NotUserTopBar";
import AdminTopBar from "@/components/admin/AdminTopBar";

Vue.use(VueRouter)

const routes = [
    {
        path: '/startPage',
        name: 'home',
        component: NotUserTopBar,
        children: [
            {
                path: '/startPage',
                name: 'StartPage',
                title: 'Стартовая страница',
                component: () => import('../views/notUser/StartPage')
            },
            {
                path: '/clan/defender',
                name: 'defender',
                title: 'Статистика защит',
                component: () => import('../views/notUser/clan/defender/Defender')
            },
            {
                path: '/clan/store',
                name: 'store',
                title: 'Статистика клан склада',
                component: () => import('../views/notUser/clan/store/Store')
            },
            {
                path: '/clan/factory',
                name: 'factory',
                title: 'Статистика Прибыль предприятий ',
                component: () => import('../views/notUser/clan/factory/Factory')
            },
            {
                path: '/stonks',
                name: 'stonks',
                title: 'Топ акций',
                component: () => import('../views/notUser/stonks/Stonks')
            },
        ]
    },
    {
        path: '/adminPanel',
        name: 'adminPanel',
        title: 'Админ панель',
        component:AdminTopBar,
        children: [
            {
                path: '/adminPanel/arts',
                name: 'adminPanel arts',
                title: 'Настройка артефактов',
                component: () => import('../views/admin/arts/Arts')
            }
        ]

    },
    {
        path: '*',
        redirect: '/startPage',
    },

]

const router = new VueRouter({
    routes
})

export default router
