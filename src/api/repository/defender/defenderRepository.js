import {client} from "@/api/configEngine/client";

class DefenderRepository {

    async getHeroesGold(dateStart, dateFinish, clanId) {
        let response = await client("getClanDefenderHeroesGoldResponse",{dateStart, dateFinish, clanId})
        return {data: response.data.heroesDef}
    }
    async getClanGold(dateStart, dateFinish, clanId) {
        let response = await client("getClanDefenderClanGoldResponse",{dateStart, dateFinish, clanId})
        return {data: response.data}
    }
    async getHeroesDef(dateStart, dateFinish, clanId) {
        let response = await client("getClanDefenderHeroesDefResponse",{dateStart, dateFinish, clanId})
        return {data: response.data.heroesDef}
    }
}
export default DefenderRepository