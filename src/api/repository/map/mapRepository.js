import {client} from "@/api/configEngine/client";

class MapRepository {
    async getAllMapSectors(){
        let response = await client("getAllMapResponse")
        return {data: response.data.region}
    }

    async getFactory(dateStart, dateEnd, clanId, region){
        let response = await client("getFactoryResponse",{dateStart, dateEnd, clanId, region})
        return {data: response.predpsArr}
    }
}
export default MapRepository
