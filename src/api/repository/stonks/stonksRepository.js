import {client} from "@/api/configEngine/client";

class StonksRepository {

    async getLastWeekStonks(){
        let response = await client("getLastWeekStonksResponse")
        return {data: response.data.predpsArr}
    }
}
export default StonksRepository