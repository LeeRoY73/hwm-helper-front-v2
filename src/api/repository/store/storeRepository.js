import {client} from "@/api/configEngine/client";

class StoreRepository {

    async getProfitClanStore(dateStart, dateFinish, clanId) {
        let response = await client("getClanProfitResponse", {dateStart, dateFinish, clanId})
        return {data: response.data}
    }

    async getExpenseClanStore(dateStart, dateFinish, clanId) {
        let response = await client("getClanExpenseResponse", {dateStart, dateFinish, clanId})
        return {data: response.data}
    }

    async getProfitTotalClanStore(dateStart, dateFinish, clanId) {
        let response = await client("getClanProfitTotalResponse", {dateStart, dateFinish, clanId})
        return {data: response.data}
    }

    async getExpenseTotalClanStore(dateStart, dateFinish, clanId) {
        let response = await client("getClanExpenseTotalResponse", {dateStart, dateFinish, clanId})
        return {data: response.data}
    }

    async getArtProfitClanStore(dateStart, dateFinish, clanId) {
        let response = await client("getClanArtProfitResponse", {dateStart, dateFinish, clanId})
        return {data: response.data}
    }

    async getBlacksmithProfitClanStore(dateStart, dateFinish, clanId) {
        let response = await client("getClanBlacksmithProfitResponse", {dateStart, dateFinish, clanId})
        return {data: response.data}
    }
}

export default StoreRepository
