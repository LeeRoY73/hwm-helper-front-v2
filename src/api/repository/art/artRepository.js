import BaseApi from "@/api/configEngine/baseUrl";

const baseUrl = BaseApi()

class ArtRepository {

    async getArtTypes() {
        let responseData
        await fetch(`${baseUrl}/art/type/get`, {
            method: 'GET',
            credentials: 'same-origin',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "Content-Type",
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
        })
            .then(async (response) => {
                responseData = await response.json();
                return responseData
            })
            .then((data) => {
                responseData = data;
            });
        return responseData.data
    }

    async crateArt(data) {
        try {
            let responseData
            await fetch(`${baseUrl}/art/create`, {
                method: 'POST',
                credentials: 'same-origin',
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Headers": "Content-Type",
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data),
                redirect: 'follow',
                referrerPolicy: 'no-referrer',
            })
                .then(async (response) => {
                    responseData = await response.json();
                    return responseData
                })
                .catch(() => {
                    responseData = {error: 'Системная ошибка'}
                });

            return responseData
        } catch (e) {
            return {error: 'Системная ошибка'}
        }
    }

    async getArts(){
        let responseData
        await fetch(`${baseUrl}/art/get`, {
            method: 'GET',
            credentials: 'same-origin',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "Content-Type",
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
        })
            .then(async (response) => {
                responseData = await response.json();
                return responseData
            })
            .then((data) => {
                responseData = data;
            });
        return responseData.data
    }

    async changeArts(data){
        let responseData
        await fetch(`${baseUrl}/art/change`, {
            method: 'post',
            credentials: 'same-origin',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "Content-Type",
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body:JSON.stringify(data),
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
        })
            .then(async (response) => {
                responseData = await response.json();
                return responseData
            })
            .then((data) => {
                responseData = data;
            });
        return responseData
    }

    async deleteArts(_id){
        let responseData
        await fetch(`${baseUrl}/art/delete`, {
            method: 'post',
            credentials: 'same-origin',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "Content-Type",
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body:JSON.stringify({'_id': _id}),
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
        })
            .then(async (response) => {
                responseData = await response.json();
                return responseData
            })
            .then((data) => {
                responseData = data;
            });
        return responseData
    }

}

export default ArtRepository
