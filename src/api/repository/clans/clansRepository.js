import {client} from "@/api/configEngine/client";


class ClansRepository {
    async getAllClans(){
        let response = await client("getAllClansResponse")
        return {data: response.data.clansAll}
    }
}
export default ClansRepository
