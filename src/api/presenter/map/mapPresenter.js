import MapRepository from "@/api/repository/map/mapRepository";
import MapController from "@/api/controller/map/mapController";

class MapPresenter {
    constructor() {
        this.repository = new MapRepository()
        this.contorller = new MapController(this.repository)
    }

    getAllMapSectors() {
        return this.contorller.getAllMapSectors()
    }

    async getFactory(data) {
        const {dateStart, dateFinish, clanId, region} = data
        let response = await this.contorller.getFactory(clanId, region, dateStart, dateFinish)

        if (response.error) {
            return response
        }
        let regionCount = {}
        for (let item of response) {
            item['clanHref'] = `https://www.heroeswm.ru/clan_info.php?id=${item.clanId}`
            item['factoryHref'] = `https://www.heroeswm.ru/object-info.php?id=${item.id}`
            if (!regionCount[item.region]) {
                regionCount[item.region] = {}
            }
            if (!regionCount[item.region][item.clanId]) {
                regionCount[item.region][item.clanId] = 1
            } else {
                regionCount[item.region][item.clanId] += 1
            }
        }
        for (let item of response) {
            item['countClanFactoryInRegion'] = regionCount[item.region][item.clanId]
            let gold = 0
            for (const itemGold of item.goldArr) {
                gold += Number(itemGold.gold)
            }
            item['totalGold'] = Math.ceil(gold)
            if (item.countClanFactoryInRegion >= 4) {
                item['totalGoldPresent'] = Math.ceil((gold / 100) * 5)
            }
            if (item.countClanFactoryInRegion < 4) {
                item['totalGoldPresent'] = Math.ceil((gold / 100) * 3)
            }
        }
        return response
    }
}

export default MapPresenter
