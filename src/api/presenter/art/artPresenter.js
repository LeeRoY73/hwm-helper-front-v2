import ArtRepository from "@/api/repository/art/artRepository";
import ArtController from "@/api/controller/art/artController";

class ArtPresenter {
    constructor() {
        this.repository = new ArtRepository()
        this.controller = new ArtController(this.repository)
    }
    getArtTypes(){
        return this.controller.getArtTypes()
    }
    crateArt(data){
        return this.controller.crateArt(data)
    }
    getArts(){
        return this.controller.getArts()
    }
    changeArts(data){
        return this.controller.changeArts(data)
    }
    deleteArts(_id){
        return this.controller.deleteArts(_id)
    }
}
export default ArtPresenter
