import StonksRepository from "@/api/repository/stonks/stonksRepository";
import StonksController from "@/api/controller/stonks/stonksController";

class StonksPresenter {
    constructor() {
        const repository = new StonksRepository()
        this.constroller = new StonksController(repository)
    }
    async getLastWeekStonks(){
        let response =  await this.constroller.getLastWeekStonks()
        let index = 1

        response.data.forEach(el=>{
            el['index'] = index
            el['url'] = 'https://www.heroeswm.ru/object_sh_info.php?id=' + el.id
            index++
        })
        return response.data
    }
}
export default StonksPresenter
