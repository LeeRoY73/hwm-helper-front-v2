import StoreRepository from "@/api/repository/store/storeRepository";
import StoreController from "@/api/controller/store/storeController";
import moment from "moment";

class StorePresenter {
    constructor() {
        this.repository = new StoreRepository()
        this.controller = new StoreController(this.repository)
    }

    async getProfitClanStore(data) {
        let response = await this.controller.getProfitClanStore(data.dateStart, data.dateFinish, data.clanId)
        let responseData = []
        response.data.forEach(el => {
            el.date = moment(el.date).format('YYYY-MM-DD')
            let findItem = responseData.find(elFind => {
                if (elFind.date === el.date)
                    return elFind
            })
            if (findItem) {
                findItem.gold += Number(el.gold)
            } else {
                responseData.push({date: el.date, gold: Number(el.gold)})
            }
        })
        responseData.sort(function (a, b) {
            return b.date - a.date
        })
        return {clanStore: responseData}
    }

    async getExpenseClanStore(data) {
        let response = await this.controller.getExpenseClanStore(data.dateStart, data.dateFinish, data.clanId)
        let responseData = []
        response.data.forEach(el => {
            el.date = moment(el.date).format('YYYY-MM-DD')
            let findItem = responseData.find(elFind => {
                if (elFind.date === el.date)
                    return elFind
            })
            if (findItem) {
                findItem.blacksmithGold += Number(el.blacksmithGold)
                findItem.repairGold += Number(el.repairGold)
            } else {
                responseData.push({
                    date: el.date,
                    repairGold: Number(el.repairGold),
                    blacksmithGold: Number(el.blacksmithGold)
                })
            }
        })
        responseData.sort(function (a, b) {
            return b.date - a.date
        })
        return {clanStore: responseData}
    }

    async getProfitTotalClanStore(data) {
        return await this.controller.getProfitTotalClanStore(data.dateStart, data.dateFinish, data.clanId)
    }

    async getExpenseTotalClanStore(data) {
        return await this.controller.getExpenseTotalClanStore(data.dateStart, data.dateFinish, data.clanId)
    }
    async getBlacksmithProfitClanStore(data) {
         let response = await this.controller.getBlacksmithProfitClanStore(data.dateStart, data.dateFinish, data.clanId)
        response.data.sort(function (a, b) {
            return b.gold - a.gold
        })
        return response
    }

    async getArtProfitClanStore(data) {
        let response = await this.controller.getArtProfitClanStore(data.dateStart, data.dateFinish, data.clanId)
        response.data.forEach(el => {
            el['total'] = el.gold - el.repairGold - el.blacksmithGold
        })
        response.data.sort(function (a, b) {
            return b.total - a.total
        })
        return response


    }
}

export default StorePresenter
