import ClansRepository from "@/api/repository/clans/clansRepository";
import ClansController from "@/api/controller/clans/clansController";

class ClansPresenter {
    constructor() {
        this.repository = new ClansRepository()
        this.controller = new ClansController(this.repository)
    }

    async getAllClans() {
        let response = await this.controller.getAllClans()
        response = response.filter(el => {
            if (el.clanTitle) {
                return el
            }
        })
        return response
    }
}

export default ClansPresenter
