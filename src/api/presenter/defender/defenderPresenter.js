import DefenderRepository from "@/api/repository/defender/defenderRepository";
import DefenderController from "@/api/controller/defender/defenderController";

class DefenderPresenter {
    constructor() {
        const repository = new DefenderRepository()
        this.constroller = new DefenderController(repository)
    }

    async getHeroesGold(dateStart, dateFinish, clanId) {
        let gold = 0
        let response = await this.constroller.getHeroesGold(dateStart, dateFinish, clanId)
        response.forEach(el => {
            gold += Number(el.data)
        })
        return gold
    }

    async getHeroesDef(dateStart, dateFinish, clanId) {
        let heroesData = []
        let goldTotal = 0;
        let response = await this.constroller.getHeroesDef(dateStart, dateFinish, clanId)
        response.forEach(el => {
            goldTotal += Number(el.gold)

            let foundFlag = false;
            heroesData.forEach(elReturn => {
                if (el.heroes === elReturn.heroesNickName) {
                    elReturn.heroesDefTotal += 1
                    if (el.pvp) {
                        elReturn.heroesPVPDefTotal += 1
                        if (el.result === "win") {
                            elReturn.heroesPVPDefWin += 1
                        } else {
                            elReturn.heroesPVPDefLose += 1
                        }
                    }
                    if (el.result === "win") {
                        elReturn.heroesDefWin += 1
                    } else {
                        elReturn.heroesDefLose += 1
                    }
                    foundFlag = true
                }
            })

            if (!foundFlag) {
                let data = {
                    heroesNickName: el.heroes,
                    heroesDefTotal: 1,
                    heroesDefWin: 0,
                    heroesDefLose: 0,
                    heroesPVPDefTotal: 0,
                    heroesPVPDefWin: 0,
                    heroesPVPDefLose: 0,
                }
                if (el.pvp) {
                    data.heroesPVPDefTotal += 1
                    if (el.result) {
                        data.heroesPVPDefWin += 1
                    } else {
                        data.heroesPVPDefLose += 1
                    }
                }
                if (el.result) {
                    data.heroesDefWin += 1
                } else {
                    data.heroesDefLose += 1
                }
                heroesData.push(data)
            }

        })
        // сортировка по кол-во боям
        heroesData.sort(function (a,b){
           return   b.heroesDefTotal - a.heroesDefTotal
        })
        let index = 1;
        heroesData.forEach(el=>{
            el['index'] = index
            index++
        })
        return {goldTotal, heroesData}
    }

    async getClanGold(dateStart, dateFinish, clanId) {
        let response = await this.constroller.getClanGold(dateStart, dateFinish, clanId);
        return {
            taxesPaid: {
                gold: response.data0.gold0,
                title: response.data0.status
            },
            taxesReceived: {
                gold: response.data2.gold2,
                title: response.data2.status
            },
            receivedForDef: {
                gold: response.data3.gold3,
                title: response.data3.status
            }
        }
    }
}

export default DefenderPresenter