import BaseApi from "@/api/configEngine/baseUrl";

const baseUrl = BaseApi()
const methods = require('./methods.json')

export async function client(methodName, data) {
    let selectMethod = methods[methodName];

    if (!selectMethod) {
        console.error(`Метод ${methodName} не найден`);
        return false;
    }

    if (selectMethod.methods.toLowerCase() === 'get') {
        if (!selectMethod.isNeedParamsName) {
            let url = `${selectMethod.url}`
            if (selectMethod.params) {
                let paramsKeys = Object.keys(selectMethod.params)
                paramsKeys.forEach(el => {
                    if (data[el]) {
                        url += `${data[el]}/`
                    }
                })
                url.substring(0, url.length - 1);
                return await fetchApiGet(url)
            }

            return await fetchApiGet(url)
        } else {

            let url = `${selectMethod.url}?`
            let paramsKeys = Object.keys(selectMethod.params)
            paramsKeys.forEach(el => {
                if (data[el]) {
                    url += `${el}=${data[el]}&`
                }
            })
            url.substring(0, url.length - 1);

            return await fetchApiGet(url)

        }
    }
}

async function fetchApiGet(url) {
    let responseData
    await fetch(`${baseUrl}/${url}`, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "Content-Type",
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
    })
        .then(async (response) => {
            responseData = await response.json();
            return responseData
        })
        .then((data) => {
            responseData = data;
        });
    return responseData
}