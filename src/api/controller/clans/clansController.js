class ClansController {
    constructor(repository) {
        this.repository = repository
    }
    async getAllClans(){
        let response = await this.repository.getAllClans()
        return response.data
    }
}
export default ClansController
