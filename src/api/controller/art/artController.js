class ArtController {
    constructor(repository) {
        this.repository = repository
    }
    getArtTypes(){
        return this.repository.getArtTypes()
    }
    crateArt(data){
        return this.repository.crateArt(data)
    }
    getArts(){
        return this.repository.getArts()
    }
    changeArts(data){
        return this.repository.changeArts(data)
    }
    deleteArts(_id){
        return this.repository.deleteArts(_id)
    }
}
export default ArtController
