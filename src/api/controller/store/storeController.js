import moment from "moment";

class StoreController {
    constructor(repository) {
        this.repository = repository
    }

    async getProfitClanStore(dateStart, dateFinish, clanId) {
        dateStart = moment(dateStart).format('MM-DD-YYYY')
        dateFinish = moment(dateFinish).format('MM-DD-YYYY')
        return this.repository.getProfitClanStore(dateStart, dateFinish, clanId)
    }
    async getExpenseClanStore(dateStart, dateFinish, clanId) {
        dateStart = moment(dateStart).format('MM-DD-YYYY')
        dateFinish = moment(dateFinish).format('MM-DD-YYYY')
        return this.repository.getExpenseClanStore(dateStart, dateFinish, clanId)
    }
    async getProfitTotalClanStore(dateStart, dateFinish, clanId) {
        dateStart = moment(dateStart).format('MM-DD-YYYY')
        dateFinish = moment(dateFinish).format('MM-DD-YYYY')
        return this.repository.getProfitTotalClanStore(dateStart, dateFinish, clanId)
    }
    async getExpenseTotalClanStore(dateStart, dateFinish, clanId) {
        dateStart = moment(dateStart).format('MM-DD-YYYY')
        dateFinish = moment(dateFinish).format('MM-DD-YYYY')
        return this.repository.getExpenseTotalClanStore(dateStart, dateFinish, clanId)
    }
    async getBlacksmithProfitClanStore(dateStart, dateFinish, clanId) {
        dateStart = moment(dateStart).format('MM-DD-YYYY')
        dateFinish = moment(dateFinish).format('MM-DD-YYYY')
        return this.repository.getBlacksmithProfitClanStore(dateStart, dateFinish, clanId)
    }
    async getArtProfitClanStore(dateStart, dateFinish, clanId) {
        dateStart = moment(dateStart).format('MM-DD-YYYY')
        dateFinish = moment(dateFinish).format('MM-DD-YYYY')
        return this.repository.getArtProfitClanStore(dateStart, dateFinish, clanId)
    }
}

export default StoreController
