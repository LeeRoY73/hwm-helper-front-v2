class DefenderController {

    constructor(repository) {
        this.repository = repository
    }

    async getHeroesGold(dateStart, dateFinish, clanId) {
        let response = await this.repository.getHeroesGold(dateStart, dateFinish, clanId)
        return response.data
    }

    async getClanGold(dateStart, dateFinish, clanId) {
        let response = await this.repository.getClanGold(dateStart, dateFinish, clanId)
        return response.data
    }

    async getHeroesDef(dateStart, dateFinish, clanId) {
        let response = await this.repository.getHeroesDef(dateStart, dateFinish, clanId)
        return response.data
    }
}

export default DefenderController