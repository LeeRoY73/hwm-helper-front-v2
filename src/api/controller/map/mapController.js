import moment from "moment";

class MapController {
    constructor(repository) {
        this.repository = repository
    }

    async getAllMapSectors() {
        let response = await this.repository.getAllMapSectors()
        return response.data
    }

    async getFactory(clanId, region, dateStart, dateFinish) {
        let response = await this.repository.getFactory(
            moment(dateStart).format("DD-MM-YYYY"),
            moment(dateFinish).format("DD-MM-YYYY"),
            clanId,
            region)
        return response.data
    }
}

export default MapController
