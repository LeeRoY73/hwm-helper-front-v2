class StonksController {

    constructor(repository) {
        this.repository = repository
    }
    async getLastWeekStonks(){
        return await this.repository.getLastWeekStonks()
    }
}
export default StonksController