export default function errorCheck(data, successKey) {
    if (data.error)
        return {error: {text: data.error, type: 'error', title: 'Ошибка!'}}
    if (data[successKey]) {
        return data[successKey]
    }
}